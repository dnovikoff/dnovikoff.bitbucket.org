const startIndex = 0;
const manIndex = startIndex;
const pinIndex = manIndex+9;
const souIndex = pinIndex+9;
const windIndex = souIndex+9;
const dragonIndex = windIndex+4;
const endIndex = dragonIndex+3;

function HandDesc() {
	this.len = [9,9,9,4,3];
	this.prefix = 'mpswd';
	this.char = '123456789123456789123456789eswnwgr';
}

function Wall(start, end) {
	this.init(start, end);
}

Wall.prototype.init = function(start, end) {
	if (!end) {
		start = startIndex;
		end = endIndex;
	}

	var result = [];
	for (var x = 0; x < 4; ++x) {
		for (var i = start; i < end; ++i) {
			result.push(i);
		}
	}
	this.wall = result;
	this.shuffle();
}

Wall.prototype.shuffle = function() {
	var arr = this.wall;
	var len = arr.length;
	for (var i = 0; i < len - 1; i++) {
		var j = rnd(i, len - 1);
		if (j != i) {
			var tmp = arr[i];
			arr[i] = arr[j];
			arr[j] = tmp;
		}
	}
}

Wall.prototype.pop = function() {
	return this.wall.pop();
}

HandDesc.prototype.repeat_ = function(str, count) {
	var r = "";
	for (var x=0; x < count; ++x) {
		r += str;
	}
	return r;
}

HandDesc.prototype.handToString = function(hand) {
	var index = 0;
	var result = "";
	for (var i=0; i < this.len.length; ++i) {
		var part = this.prefix.charAt(i);
		for (var x=0; x < this.len[i]; ++x) {
			var cur = this.char.charAt(index);
			var count = hand[index];
			if (count > 0) {
				part += this.repeat_(cur, count);
			}
			++index;
		}
		if (part.length > 1) {
			if (result != "") {
				result += " ";
			}
			result += part;
		}
	}
	return result;
}

var handDesc = new HandDesc();

function Hand () {
	this.clear();
}

function Stack() {
	this.stack = [];
	this.missing = [];
	this.length = 0;
	this.inHand = [];
}

Stack.prototype.normalizeMissing = function() {
	var result = [];
	for (var i=0; i < this.missing.length ; ++i) {
		result = $.merge(result, this.missing[i]);
	}
	result = $.merge(result, this.inHand);
	result = sort_unique(result);
	return result;
}

Stack.prototype.addMissing = function(m) {
	if (m == undefined) {
		m = [];
	} else if (m.length == undefined) {
		m = [m];
	}
	this.missing.push(m); 
}

Stack.prototype.setInHand = function(h) {
	var result = [];
	for (var i=0; i < h.uniqueTilesCount(); ++i) {
		for (var j=0; j < h.hand[i]; ++j) {
			result.push(i);
		}
	}
	this.inHand = result;
}

Stack.prototype.mergeMissing = function(x) {
	this.inHand = $.merge(this.inHand, x.inHand);
	this.missing = $.merge(this.missing, x.missing);
}

Stack.prototype.push = function(x, missing) {
	this.addMissing(missing);
	this.stack.push(x);
	this.length = this.stack.length;
}

Stack.prototype.pop = function() {
	this.missing.pop();
	return this.stack.pop();
}

Hand.prototype.sameTilesAs = function(other) {
	for (var i=0; i < this.uniqueTilesCount(); ++i) {
		var l = (this.hand[i]>0);
		var r = (other.hand[i]>0);
		if (l != r) {
			return false;
		}
	}
	return true;
}

Hand.prototype.uniqueTilesCount = function() {
	return handDesc.char.length;
}

Hand.prototype.clean_ = function() {
	var hand = [];
	for (var i=0; i < this.uniqueTilesCount(); ++i) {
		hand.push(0);
	}
	return hand;
}

Hand.prototype.clear = function() {
	this.hand = this.clean_();
	this.bestStacks = [];
	this.bestShanten = 0;
}

Hand.prototype.toString = function() {
	return handDesc.handToString(this.hand);
}

Hand.prototype.fillFromWall = function(wall, count) {
	if (!count) {
		count = 13;
	}
	while ((count--) > 0) {
		++this.hand[wall.pop()];
	}
}

Hand.prototype.isHonor = function(x) {
	return x >= 9*3;
}

Hand.prototype.isTerm = function(x) {
	var num = x%9;
	return !this.isHonor(x) && (num == 0 || num == 8);
}

Hand.prototype.isTermOrHonor = function(x) {
	return this.isHonor(x) || this.isTerm(x);
}

Hand.prototype.isMiddle = function(x) {
	return !this.isTerm(x) && !this.isHonor(x);
}

Hand.prototype.shanten7_ = function() {
	var stack = new Stack();
	for (var i = 0; i < this.hand.length; ++i) {
		if (this.hand[i] > 1) {
			stack.push([i,i]);
		} else if (this.hand[i] == 1) {
			stack.addMissing(i);
		}
	}
	var result = 6 - stack.length;
	if (result < this.bestShanten) {
		this.bestShanten = result;
		this.bestStacks = [stack];
	}
	return result;
}

Hand.prototype.shanten13_ = function() {
	var pairCounted = false;
	var stack = new Stack();
	var all = new Stack();
	for (var i = 0; i < this.uniqueTilesCount(); ++i) {
		var cur = this.hand[i];
		if (this.isMiddle(i)) {
			continue;
		}
		all.addMissing(i);
		if (cur == 0) {
			stack.addMissing(i);
			continue;
		}
		if (cur > 1 && !pairCounted) {
			pairCounted = true;
			stack.push(i);
		}
		stack.push(i);
	}
	
	if (!pairCounted) {
		stack.mergeMissing(all);
	}

	var result = 13-stack.length;
	if (result < this.bestShanten) {
		this.bestShanten = result;
		this.bestStacks = [[stack]];
	}
	return result;
}

Hand.prototype.removeChi_ = function(x) {
	// Chi tail is not honor
	if (this.isHonor(x+2)) {
		return;
	}
	// Chi tail is same sequence
	if (x%9 > (x+2) %9) {
		return;
	}

	var indexes = [];
	var missing = [];
	for (var s=0; s < 3; ++s) {
		var i = x+s;
		if (this.hand[i] > 0) {
			indexes.push(i);
		} else {
			missing.push(i);
		}
	}

	if (indexes.length < 2) {
		return;
	}

	this.push_(indexes, missing);
	this.shantenNoPair_();
	this.pop_();
}

Hand.prototype.shantenNoPair_ = function() {
	// max sets count in hand is four
	if (this.setsCount >= 4) {
		return 0;
	}

	for (var i = 0; i < this.uniqueTilesCount(); ++i) {
		this.removePon_(i);
		this.removeChi_(i);
	}
}

Hand.prototype.removePon_ = function(x) {
	if (this.hand[x] < 2 || this.pairIndex == x) {
		return;
	}
	// remove
	var old = this.hand[x];

	if (this.hand[x] > 2) {
		this.push_([x, x, x], []);
	} else {
		this.push_([x, x], [x]);
	}

	this.shantenNoPair_();
	this.pop_();
}

function stacktrace() { 
	var err = new Error();
	return err.stack;
}

Hand.prototype.push_ = function(args, missing) {
	for (var i = 0; i < args.length; ++i) {
		--this.hand[args[i]];
	}
	++this.setsCount;

	this.result += (args.length-1);
	this.stack.push(args, missing);

	if (this.result < this.bestResult) {
		return;
	}

	this.stack.setInHand(this);
	if (this.result == this.bestResult) {
		this.bestStacks.push($.extend(true, [], this.stack));
	} else {
		this.bestResult = this.result;
		this.bestStacks = [$.extend(true, [], this.stack)];
	}
}

Hand.prototype.pop_ = function() {
	var args = this.stack.pop();
	for (var i = 0; i < args.length; ++i) {
		++this.hand[args[i]];
	}
	this.result -= (args.length-1);
	--this.setsCount;
}

function _indexesToString(indexes) {
	var tmp = new Hand();
	for (var j = 0; j < indexes.length; ++j) {
		tmp.hand[indexes[j]]++;
	}
	return tmp.toString();
}

function _stackToString(stack) {
	var result = "";
	for (var i = 0; i < stack.length; ++i) {
		if (result != "") {
			result += " ";
		}
		result += _indexesToString(stack[i]);
	}
	return result;
}

function sort_unique(arr) {
	return arr.sort(function(a,b){
		return (a > b) ? 1 : -1;
	}).filter(function(el,i,a) {
		return (i==a.indexOf(el));
	});
}

function arrLess(a, b) {
	var len = Math.min(a.length, b.length);
	for (var i=0; i < len; ++i) {
		if (a[i] < b[i]) {
			return false;
		} else if (a[i] > b[i]) {
			return true;
		}
	}
	return a.length < b.length;
}

Hand.prototype.bestStacksToStrings = function() {
	if (!this.bestStacks) {
		this.shanten();
	}
	var result = [];

	var isTempai = (this.bestShanten == 0); 
	
	for (var i = 0; i < this.bestStacks.length; ++i) {
		var stack = $.extend(true, [], this.bestStacks[i]);
		if (isTempai && stack.stack.length == 4 && stack.inHand.length == 1) {
			stack.push(stack.inHand, stack.inHand);
		}
		result.push(_stackToString(stack.stack.sort(function(a,b) {
			return arrLess(a, b) ? 1 : -1;
		})));
	}
	return sort_unique(result);
}

Hand.prototype.bestOuts = function() {
	if (!this.bestStacks) {
		this.shanten();
	}
	var stack = new Stack();
	for (var i = 0; i < this.bestStacks.length; ++i) {
		stack.mergeMissing(this.bestStacks[i]);
	}
	return stack.normalizeMissing();
}

Hand.prototype.bestOutsString = function() {
	return _indexesToString(this.bestOuts());
}

Hand.prototype.shantenNormal_ = function() {
	this.stack = new Stack();
	this.setsCount = 0;
	this.result = 0;
	this.bestResult = 8 - this.bestShanten;
	this.pairIndex = -1;

	// Test for no start pairs in hand
	this.shantenNoPair_();

	// Pair should not be counted as a set
	this.setsCount = -1;
	// every tile could be a pair
	for (var i = 0; i < this.uniqueTilesCount(); ++i) {
		var cur = this.hand[i];
		// Skip non pairs
		if (cur < 2) {
			continue;
		}
		// Avoid kan problems. See removePon
		this.pairIndex = i;

		this.push_([i, i],[]);
		this.shantenNoPair_();
		this.pop_();
	}
	var result = 8 - this.bestResult;
	if (result < this.bestShanten) {
		this.bestShanten = result;
	}
	return result;
}

Hand.prototype.shanten = function() {
	this.bestShanten = 7;
	this.shanten7_();
	this.shanten13_();
	this.shantenNormal_();
	return this.bestShanten;
}

Hand.prototype.fromString = function(x) {
	x += " ";
	var selector = -1;

	var hand = this.clean_();
	var cnt = 0;
	for (var i=0; i < x.length; ++i) {
		var c = x.charAt(i);

		if (c == " ") {
			selector = -1;
			continue;
		}
		
		if (selector == -1) {
			if (c == "m") {
				selector = manIndex;
			} else if (c  == "p") {
				selector = pinIndex;
			} else if (c == "s") {
				selector = souIndex;
			} else if (c == "w") {
				selector = windIndex;
			} else if (c == "d") {
				selector = dragonIndex;
			} else {
				return false;
			}
			continue;
		}

		var num = 0;

		if (selector < windIndex) {
			if (c >= '1' && c <= '9') {
				num = parseInt(c) -1;
			} else {
				return false;
			}
		} else if (selector == windIndex) {
			if (c == "e") {
				num = 0;
			} else if (c == "s") {
				num = 1;
			} else if (c == "w") {
				num = 2;
			} else if (c == "n") {
				num = 3;
			} else {
				return false;
			}
		} else if (selector == dragonIndex) {
			if (c == "w") {
				num = 0;
			} else if (c == "g") {
				num = 1;
			} else if (c == "r") {
				num = 2;
			} else {
				return false;
			}
		}
		++hand[selector+num];
		++cnt;
		if (cnt == 13) {
			break;
		}
	}
	this.clear();
	this.hand = hand;
	return true;
}
