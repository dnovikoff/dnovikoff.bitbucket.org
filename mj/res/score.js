function Point(han, fu) {
	this.han = han;
	this.fu = fu;
}

function PointAnswer(point, isDealer, isTsumo) {
	this.point = point;
	this.isDealer = isDealer;
	this.isTsumo = isTsumo;
}

PointAnswer.prototype.special = function () {
	var han = this.point.han;
	var special = "";

	if (han > 12) {
		special = " якуман";
	} else if (han > 10) {
		special = " санбайман";
	} else if (han > 7) {
		special = " байман";
	} else if (han > 5) {
		special = " ханеман";
	} else if (han == 5 || this.point.mul(4) == 8000) {
		special = " манган";
	}
	return special;
}

PointAnswer.prototype.desc = function () {
	var points = this.point.han;
	if (this.point.fu) {
		points += "." + this.point.fu;
	}
	return points + " по "+(this.isTsumo?"цумо":"рон") + (this.isDealer?"+дилер":"");
}

PointAnswer.prototype.string = function () {
	if (this.isDealer) {
		return this.point.mul(this.isTsumo?2:6);
	}
	if (this.isTsumo) {
		return this.point.mul(2) + "/" + this.point.mul(1);
	}
	return this.point.mul(4);
}

Point.prototype.mul = function(m) {
	return this.round100(this.base() * m);
}

Point.prototype.round100 = function (x) {
	return Math.ceil(x/100) * 100;
}

Point.prototype.base = function () {
	var han = this.han;
	var fu = this.fu;
	if (han > 12) {
		return 8000;
	}
	if (han > 10) {
		return 6000;
	}
	if (han > 7) {
		return 4000;
	}
	if (han > 5) {
		return 3000;
	}
	if (han == 5) {
		return 2000;
	}
	var result = fu * Math.pow(2, 2 + han);
	if (result > 2000) {
		return 2000;
	}
	if (result < 300) {
		return 300;
	}
	return result;
};

function PointTable() {
	this.init();
}

PointTable.prototype.push = function (p) {
	this.pushRon(p);
	this.pushTsumo(p);
}

PointTable.prototype.push = function (p) {
	this.pushRon(p);
}

PointTable.prototype.pushTsumo = function(p) {
	this.table.push(new PointAnswer(p, true, true));
	this.table.push(new PointAnswer(p, false, true));
}

PointTable.prototype.pushRon = function(p) {
	this.table.push(new PointAnswer(p, true, false));
	this.table.push(new PointAnswer(p, false, false));
}

PointTable.prototype.fill = function(han, fuBegin, fuEnd) {
	for (var i=fuBegin; i <= fuEnd; ) {
		this.push(new Point(han, i));
		if (i == 25) {
			i = 30;
		} else {
			i += 10;
		}
	}
}

PointTable.prototype.fillHan = function(hanStart, hanEnd) {
	for (var i=hanStart; i <= hanEnd; ++i) {
		this.push(new Point(i, 0));
	}
}

PointTable.prototype.getRandomAnswer = function() {
	return this.table[rnd(0, this.table.length-1)];
}

PointTable.prototype.init = function() {
	this.table = [];
	this.fill(1, 30, 110);

	this.pushTsumo(new Point(2, 20));
	this.pushRon(new Point(2, 25));
	this.fill(2, 30, 110);
	this.pushTsumo(new Point(3, 20));
	this.fill(3, 25, 70);
	this.pushTsumo(new Point(4, 20));
	this.fill(4, 25, 40);
	
	this.fillHan(5, 13);
}